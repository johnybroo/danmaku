﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace SFML
{
    class GamePlay : GameState
    {
        Player player;
        Emitter emitter;
        CollisionManager collisionManager;

        public GamePlay()
        {
            player = new Player();
            emitter = new Emitter();
            collisionManager = new CollisionManager();
            emitter.BulletsUpdated += collisionManager.Emitter_BulletsUpdated;
            Game.window.MouseWheelMoved += Window_MouseWheelMoved;
        }

        private void Window_MouseWheelMoved(object sender, MouseWheelEventArgs e)
        {
            View view = Game.view;
            if (e.Delta == -1)
            {
                view.Zoom(1.1f);
            }
            if (e.Delta == 1)
            {
                view.Zoom(0.9f);
            }
            Game.window.SetView(view);
        }

        public override void Update(Time elapsedTime)
        {
            collisionManager.Update(elapsedTime);
            checkInput(elapsedTime);
            player.Update(elapsedTime);
            emitter.Update(elapsedTime);
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(emitter);
            target.Draw(player);
        }

        public override void pause()
        {

        }

        public override void resume()
        {

        }

        public override void close()
        {

        }

        protected override void checkInput(Time elapsedTime)
        {
            View view = Game.view;
            Vector2f viewOffset = new Vector2f(0, 0);
            int speedOffset = 100;

            if (Keyboard.IsKeyPressed(Keyboard.Key.Left))
            {
                viewOffset.X -= speedOffset * elapsedTime.AsSeconds();
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.Right))
            {
                viewOffset.X += speedOffset * elapsedTime.AsSeconds();
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.Up))
            {
                viewOffset.Y -= speedOffset * elapsedTime.AsSeconds();
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.Down))
            {
                viewOffset.Y += speedOffset * elapsedTime.AsSeconds();
            }

            if (Keyboard.IsKeyPressed(Keyboard.Key.Q))
            {
                view.Rotate(-30 * elapsedTime.AsSeconds());
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.E))
            {
                view.Rotate(30 * elapsedTime.AsSeconds());
            }

            view.Move(viewOffset);
            Game.window.SetView(view);
        }
    }
}
