﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SFML
{
    class GameStateManager : Drawable, Updatable
    {
        public static GameStateManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameStateManager();
                }
                return instance;
            }
        }//Singleton

        private static GameStateManager instance;

        Stack<GameState> gameStateStack;
        int fpsCount = 0;
        float timeFps = 0;
        Font fpsFont;
        Text fpsText;

        private GameStateManager()
        {
            gameStateStack = new Stack<GameState>();
            push(new GamePlay());
            fpsFont = RessourceManager.Instance.createFont("PIXEARG_.TTF");
            fpsText = new Text("FPS", fpsFont);
            fpsText.Position = new Vector2f(10, 10);
        }

        public void Update(Time elapsedTime)
        {
            gameStateStack.Peek().Update(elapsedTime);

            if((timeFps+=elapsedTime.AsSeconds()) >= 1)
            {
                fpsText.DisplayedString = fpsCount + " FPS";
                fpsCount = 0;
                timeFps = 0;
            }
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(gameStateStack.Peek());
            target.Draw(fpsText);
            fpsCount++;
        }

        public void push(GameState gameState)
        {
            if(gameStateStack.Count > 0)
            {
                gameStateStack.Peek().pause();
            }
            gameStateStack.Push(gameState);
        }

        public void pop()
        {
            gameStateStack.Peek().close();
            gameStateStack.Pop();
            gameStateStack.Peek().resume();
        }
    }
}
