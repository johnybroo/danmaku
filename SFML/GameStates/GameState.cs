﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.Graphics;
using SFML.System;

namespace SFML
{
    public abstract class GameState : Drawable, Updatable
    {
        public abstract void Update(Time elapsedTime);
        public abstract void Draw(RenderTarget target, RenderStates states);
        protected abstract void checkInput(Time elapsedTime);
        public abstract void close();
        public abstract void pause();
        public abstract void resume();
    }
}
