﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SFML
{
    class QuadTree : Drawable
    {
        int maxObjects;
        int maxLevels;
        int level;
        List<FloatRect> objects;
        FloatRect bounds;
        QuadTree[] nodes;
        VertexArray vertexArray;

        public QuadTree(int level, FloatRect bounds)
        {
            maxObjects = 10;
            maxLevels = 5;
            this.level = level;
            this.objects = new List<FloatRect>();
            this.bounds = bounds;
            this.nodes = new QuadTree[4];
            vertexArray = new VertexArray(PrimitiveType.LinesStrip, 5);
        }

        public void Clear()
        {
            objects.Clear();

            for (int i = 0; i < nodes.Length; i++)
            {
                if (nodes[i] != null)
                {
                    nodes[i].Clear();
                    nodes[i] = null;
                }
            }
        }

        public void Split()
        {
            int subWidth = (int)bounds.Width / 2;
            int subHeight = (int)bounds.Height / 2;

            int x = (int)bounds.Left;
            int y = (int)bounds.Top;

            nodes[0] = new QuadTree(level + 1, new FloatRect(x + subWidth, y, subWidth, subHeight));
            nodes[1] = new QuadTree(level + 1, new FloatRect(x, y, subWidth, subHeight));
            nodes[2] = new QuadTree(level + 1, new FloatRect(x, y + subHeight, subWidth, subHeight));
            nodes[3] = new QuadTree(level + 1, new FloatRect(x + subWidth, y + subHeight, subWidth, subHeight));

        }

        private int getIndex(FloatRect rect)
        {
            int index = -1;

            float verticalMidPoint = bounds.Left + (bounds.Width / 2);
            float horizontalMidpoint = bounds.Top + (bounds.Height / 2);

            bool topQuadrant = (rect.Top < horizontalMidpoint && rect.Top + rect.Height < horizontalMidpoint);
            bool botQuadrant = (rect.Top > horizontalMidpoint);

            // Object can completely fit within the left quadrants
            if (rect.Left < verticalMidPoint && rect.Left + rect.Width < verticalMidPoint)
            {
                if (topQuadrant)
                {
                    index = 1;
                }
                else if (botQuadrant)
                {
                    index = 2;
                }
            }
            // Object can completely fit within the right quadrants
            else if (rect.Left > verticalMidPoint)
            {
                if (topQuadrant)
                {
                    index = 0;
                }
                else if (botQuadrant)
                {
                    index = 3;
                }
            }
            return index;
        }

        public void insert(FloatRect rect)
        {
            if (nodes[0] != null)
            {
                int index = getIndex(rect);

                if (index != -1)
                {
                    nodes[index].insert(rect);

                    return;
                }
            }

            objects.Add(rect);

            if (objects.Count > maxObjects && level < maxLevels)
            {
                if (nodes[0] == null)
                {
                    Split();
                }

                int i = 0;
                while (i < objects.Count)
                {
                    int index = getIndex(objects[i]);
                    if (index != -1)
                    {
                        nodes[index].insert(objects[i]);
                        objects.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
            }
        }

        public void retrieve(ref List<FloatRect> returnObjects, FloatRect pRect)
        {
            int index = getIndex(pRect);
            if (index != -1 && nodes[0] != null)
            {
                nodes[index].retrieve(ref returnObjects, pRect);
            }

            returnObjects.AddRange(objects);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            Vertex vertex1 = new Vertex(new Vector2f(bounds.Left, bounds.Top), Color.Red);
            Vertex vertex2 = new Vertex(new Vector2f(bounds.Width, bounds.Top), Color.Red);
            Vertex vertex3 = new Vertex(new Vector2f(bounds.Width, bounds.Height), Color.Red);
            Vertex vertex4 = new Vertex(new Vector2f(bounds.Left, bounds.Height), Color.Red);
            Vertex vertex5 = vertex1;

            vertexArray[0] = vertex1;
            vertexArray[1] = vertex2;
            vertexArray[2] = vertex3;
            vertexArray[3] = vertex4;
            vertexArray[4] = vertex5;

            target.Draw(vertexArray);

            for (int i = 0; i < nodes.Length; i++)
            {
                if (nodes[i] != null)
                {
                    target.Draw(nodes[i]);
                }
            }
        }
    }
}
