﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SFML
{
    class MyMath
    {
        public static void normalize(ref Vector2f vector)
        {
            if (vector.X != 0 && vector.Y != 0)
            {
                float mag = magnitude(vector);
                vector.X /= mag;
                vector.Y /= mag;
            }
        }

        public static float magnitude(Vector2f vector)
        {
            return (float)Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y);
        }

        public static void rotateVector(ref Vector2f vector, float degree)
        {
            vector.X = (float)(vector.X * Math.Cos(MyMath.DegreeToRadian(degree)) - vector.Y * Math.Sin(MyMath.DegreeToRadian(degree)));
            vector.Y = (float)(vector.X * Math.Sin(MyMath.DegreeToRadian(degree)) + vector.Y * Math.Cos(MyMath.DegreeToRadian(degree)));
        }

        public static float RadianToDegree(float radian)
        {
            return (float)(radian * (180.0 / Math.PI));
        }

        public static float DegreeToRadian(float degree)
        {
            return (float)(Math.PI * degree / 180.0);
        }
    }
}