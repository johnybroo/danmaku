﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.System;
using SFML.Window;

namespace SFML
{
    /// <summary>
    /// Entitée qui permet de créer un pattern du projectile
    /// </summary>
    class Emitter : Transformable, Drawable, Updatable
    {
        public event EventHandler<BulletsEventArgs> BulletsUpdated;

        float fireRate = 0.1f;
        float spawnTimer = 0;
        float bulletSpeed = 500f;
        Vector2f bulletSize;
        Texture bulletTexture;
        List<Bullet> bulletList;
        Vector2f bulletDirection;
        VertexArray vertexArrayBullets;
        VertexArray vertexArrayEmitter;

        public Emitter()
        {
            bulletSize = new Vector2f(50f, 50f);
            bulletDirection = new Vector2f(-1, 0);
            bulletTexture = RessourceManager.Instance.createTexture("bullet.png");
            vertexArrayBullets = new VertexArray(PrimitiveType.Quads);
            vertexArrayEmitter = new VertexArray(PrimitiveType.Quads, 4);
            bulletList = new List<Bullet>();
            Position = new Vector2f((Game.WIDTH / 2) - (bulletSize.X / 2), (Game.HEIGHT / 2) - (bulletSize.Y / 2));

            vertexArrayEmitter[0] = new Vertex(new Vector2f(0, 0), new Vector2f(0, 0));
            vertexArrayEmitter[1] = new Vertex(new Vector2f(bulletSize.X, 0), new Vector2f(bulletTexture.Size.X, 0));
            vertexArrayEmitter[2] = new Vertex(new Vector2f(bulletSize.X, bulletSize.Y), new Vector2f(bulletTexture.Size.X, bulletTexture.Size.Y));
            vertexArrayEmitter[3] = new Vertex(new Vector2f(0, bulletSize.Y), new Vector2f(0, bulletTexture.Size.Y));
        }

        public void Update(Time elapsedTime)
        {
            checkSpawn(elapsedTime);
            updateBullets(elapsedTime);
            updateBulletVertexArray(elapsedTime);
            OnBulletsUpdated();
        }

        private void updateBullets(Time elapsedTime)
        {
            for (int i = 0; i < bulletList.Count; i++)
            {
                Bullet bullet = bulletList[i];

                FloatRect bulletRect = bullet.getAABB();
                FloatRect gameRect = new FloatRect(0, 0, Game.WIDTH, Game.HEIGHT);
                if (!bulletRect.Intersects(gameRect))
                {
                    bulletList.RemoveAt(i);
                    return;
                }

                bullet.Update(elapsedTime);
            }
        }

        /// <summary>
        /// Met à jour le tableau de vertex pour chaque Bullet dans la liste
        /// </summary>
        private void updateBulletVertexArray(Time elapsedTime)
        {
            vertexArrayBullets.Resize((uint)bulletList.Count * 4);

            for (uint i = 0; i < bulletList.Count; i++)
            {
                Bullet bullet = bulletList[(int)i];

                vertexArrayBullets[i * 4 + 0] = new Vertex(bullet.position, new Vector2f(0, 0));
                vertexArrayBullets[i * 4 + 1] = new Vertex(bullet.position + new Vector2f(bulletSize.X, 0), new Vector2f(bulletTexture.Size.X, 0));
                vertexArrayBullets[i * 4 + 2] = new Vertex(bullet.position + new Vector2f(bulletSize.X, bulletSize.Y), new Vector2f(bulletTexture.Size.X, bulletTexture.Size.Y));
                vertexArrayBullets[i * 4 + 3] = new Vertex(bullet.position + new Vector2f(0, bulletSize.Y), new Vector2f(0, bulletTexture.Size.Y));
            }
        }

        /// <summary>
        /// Vérifie si il faut faire spawn des projectiles
        /// </summary>
        private void checkSpawn(Time elapsedTime)
        {
            spawnTimer += elapsedTime.AsSeconds();
            if (spawnTimer >= fireRate)
            {
                spawnTimer = 0;
                bulletList.Add(new Bullet(bulletDirection, Position, bulletSpeed, new Vector2f(bulletTexture.Size.X, bulletTexture.Size.Y)));
                MyMath.rotateVector(ref bulletDirection, 10);
                MyMath.normalize(ref bulletDirection);
            }
        }

        /// <summary>
        /// Ajoute les vertex nécessaire pour un projectile, la position du projectile est la position de l'emitter.
        /// </summary>
        private void addBulletVertex()
        {
            vertexArrayBullets.Append(new Vertex(Position, new Vector2f(0, 0)));
            vertexArrayBullets.Append(new Vertex(Position + new Vector2f(bulletSize.X, 0), new Vector2f(bulletTexture.Size.X, 0)));
            vertexArrayBullets.Append(new Vertex(Position + new Vector2f(bulletSize.X, bulletSize.Y), new Vector2f(bulletTexture.Size.X, bulletTexture.Size.Y)));
            vertexArrayBullets.Append(new Vertex(Position + new Vector2f(0, bulletSize.Y), new Vector2f(0, bulletTexture.Size.Y)));
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Texture = bulletTexture;
            target.Draw(vertexArrayBullets, states);
            states.Transform *= Transform;
            target.Draw(vertexArrayEmitter, states);
        }

        protected virtual void OnBulletsUpdated()
        {
            BulletsUpdated?.Invoke(this, new BulletsEventArgs() { bullets = bulletList });
        }
    }
}
