﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SFML
{
    class CollisionManager : Updatable
    {
        private List<FloatRect> objects;

        public CollisionManager()
        {
            objects = new List<FloatRect>();
        }

        public bool testCollision(FloatRect rect)
        {
            foreach (FloatRect o in objects)
            {
                if (rect.Intersects(o))
                {
                    return true;
                }
            }
            return false;
        }

        public void Emitter_BulletsUpdated(Object sender, BulletsEventArgs e)
        {
            foreach (Bullet bullet in e.bullets)
            {
                objects.Add(bullet.getAABB());
            }
        }

        public void Update(Time elapsedTime)
        {
            objects.Clear();
        }
    }
}
