﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.System;
using SFML.Window;

namespace SFML
{
    class Player : Transformable, Updatable, Drawable, HasFloatRect
    {
        float speed = 300;
        float spawnTimer = 0;
        float fireRate = 0.1f;
        float projectileSpeed = 2000f;
        int projectileSpawnNumber = 5;
        float projectileSpawnWidth = 100f;
        Sprite playerSprite;
        Vector2f playerSize;
        Texture playerTexture;
        Vector2f projectileSize;
        Texture projectilesTexture;
        List<Bullet> projectilesList;
        VertexArray vertexArrayProjectiles;

        public Player()
        {
            playerTexture = RessourceManager.Instance.createTexture("player.png");
            projectilesTexture = RessourceManager.Instance.createTexture("bullet.png");
            playerSprite = new Sprite(playerTexture);
            playerSize = new Vector2f(playerTexture.Size.X, playerTexture.Size.Y);
            projectileSize = new Vector2f(20, 20);
            vertexArrayProjectiles = new VertexArray(PrimitiveType.Quads);
            projectilesList = new List<Bullet>();
            Origin = new Vector2f(playerSize.X / 2, playerSize.Y / 2);
            Transform transform = new Transform();
        }

        public void Update(Time elapsedTime)
        {
            checkInput(elapsedTime);
            updateProjectiles(elapsedTime);
            checkProjectileSpawn(elapsedTime);
            updateProjectilesVertexArray();
        }

        private void checkProjectileSpawn(Time elapsedTime)
        {
            spawnTimer += elapsedTime.AsSeconds();

            if (spawnTimer >= fireRate)
            {
                spawnTimer = 0;

                Vector2f positionProjectilesSpawn = new Vector2f(Position.X - ((projectileSpawnWidth / 2) + projectileSize.X + Origin.X), Position.Y);
                float offsetX = projectileSpawnWidth / (projectileSpawnNumber - 1);

                for (int i = 0; i < projectileSpawnNumber; i++)
                {
                    positionProjectilesSpawn.X += offsetX;
                    projectilesList.Add(new Bullet(new Vector2f(0, -1), positionProjectilesSpawn, projectileSpeed, new Vector2f(20, 20)));
                }
            }
        }

        private void updateProjectiles(Time elapsedTime)
        {
            for (int i = 0; i < projectilesList.Count; i++)
            {
                Bullet projectile = projectilesList[i];

                FloatRect projectileRect = projectile.getAABB();
                FloatRect gameRect = new FloatRect(0, 0, Game.WIDTH, Game.HEIGHT);
                if (!projectileRect.Intersects(gameRect))
                {
                    projectilesList.RemoveAt(i);
                    return;
                }

                projectile.Update(elapsedTime);
            }
        }

        private void updateProjectilesVertexArray()
        {
            vertexArrayProjectiles.Resize((uint)projectilesList.Count * 4);

            for (uint i = 0; i < projectilesList.Count; i++)
            {
                Bullet projectile = projectilesList[(int)i];

                vertexArrayProjectiles[i * 4 + 0] = new Vertex(projectile.position, new Vector2f(0, 0));
                vertexArrayProjectiles[i * 4 + 1] = new Vertex(projectile.position + new Vector2f(projectileSize.X, 0), new Vector2f(projectilesTexture.Size.X, 0));
                vertexArrayProjectiles[i * 4 + 2] = new Vertex(projectile.position + new Vector2f(projectileSize.X, projectileSize.Y), new Vector2f(projectilesTexture.Size.X, projectilesTexture.Size.Y));
                vertexArrayProjectiles[i * 4 + 3] = new Vertex(projectile.position + new Vector2f(0, projectileSize.Y), new Vector2f(0, projectilesTexture.Size.Y));
            }
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Texture = projectilesTexture;
            target.Draw(vertexArrayProjectiles, states);
            states.Transform *= Transform;
            states.Texture = playerTexture;
            target.Draw(playerSprite, states);
        }

        private void checkInput(Time elapsedTime)
        {
            Vector2f direction = new Vector2f(0, 0);

            if (Keyboard.IsKeyPressed(Keyboard.Key.W))
            {
                direction += new Vector2f(0, -1);
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.A))
            {
                direction += new Vector2f(-1, 0);
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.S))
            {
                direction += new Vector2f(0, 1);
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.D))
            {
                direction += new Vector2f(1, 0);
            }

            MyMath.normalize(ref direction);
            Position += direction * speed * elapsedTime.AsSeconds();
        }

        public FloatRect getAABB()
        {
            return new FloatRect(Position, new Vector2f(playerSize.X, playerSize.Y));
        }
    }
}
