﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SFML
{
    interface Updatable
    {
        void Update(Time elapsedTime);
    }
}
