﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.System;

namespace SFML
{
    class Bullet : Updatable, HasFloatRect
    {
        public Vector2f direction;
        public Vector2f position;
        public float speed;
        public Vector2f size;

        public Bullet(Vector2f direction, Vector2f position, float speed, Vector2f size)
        {
            this.direction = direction;
            this.position = position;
            this.speed = speed;
            this.size = size;
        }

        public void Update(Time elapsedTime)
        {
            position += direction * speed * elapsedTime.AsSeconds();
        }

        public FloatRect getAABB()
        {
            return new FloatRect(position, new Vector2f(size.X, size.Y));
        }
    }
}
