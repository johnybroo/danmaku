﻿
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SFML
{
    class RessourceManager
    {
        public static RessourceManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RessourceManager();
                }
                return instance;
            }
        }//Singleton

        private static RessourceManager instance;

        String resDir;
        String textureDir;
        String fontDir;

        private RessourceManager()
        {
            resDir = @".\res\";
            textureDir = resDir + @"texture\";
            fontDir = resDir + @"font\";
        }

        public Texture createTexture(String fileName)
        {
            return new Texture(textureDir + fileName);
        }

        public Font createFont(String fileName)
        {
            return new Font(fontDir + fileName);
        }
    }
}
