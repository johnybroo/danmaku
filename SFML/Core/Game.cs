﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace SFML
{
    class Game : Updatable
    {
        public static uint WIDTH = 1920;
        public static uint HEIGHT = 1080;

        public static RenderWindow window;
        public static View view;
        Clock clock;

        public Game()
        {
            window = new RenderWindow(new VideoMode(1920, 1080), "SFML");
            window.Closed += Window_Closed;

            view = new View(new FloatRect(0, 0, WIDTH, HEIGHT));
            window.SetView(view);

            clock = new Clock();

            while (window.IsOpen)
            {
                window.DispatchEvents();
                Update(clock.Restart());
                window.Clear(Color.Black);
                draw();
                window.Display();
            }
        }

        private void Window_Closed(Object sender, EventArgs e)
        {
            window.Close();
            Environment.Exit(0);
        }

        public void Update(Time elapsedTime)
        {
            GameStateManager.Instance.Update(elapsedTime);
        }

        public void draw()
        {
            window.Draw(GameStateManager.Instance);
        }
    }
}
