﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SFML
{
    class BulletsEventArgs : EventArgs
    {
        public List<Bullet> bullets { get; set; }
    }
}
