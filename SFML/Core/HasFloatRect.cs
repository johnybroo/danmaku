﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SFML
{
    interface HasFloatRect
    {
        FloatRect getAABB();
    }
}
