﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SFML.Core
{
    class QuadTreeTest
    {
        int maxObjects;
        int maxLevels;
        int level;
        List<FloatRect> objects;
        FloatRect bounds;
        QuadTree[] nodes;
        VertexArray vertexArray;

        public QuadTreeTest(int level, FloatRect bounds)
        {
            maxObjects = 10;
            maxLevels = 5;
            this.level = level;
            this.objects = new List<FloatRect>();
            this.bounds = bounds;
            this.nodes = new QuadTree[4];
            vertexArray = new VertexArray(PrimitiveType.LinesStrip, 5);
        }

        public void Clear()
        {
            objects.Clear();

            for (int i = 0; i < nodes.Length; i++)
            {
                if (nodes[i] != null)
                {
                    nodes[i].Clear();
                    nodes[i] = null;
                }
            }
        }
    }
}
